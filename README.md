### Caren RPM

This project was Developed using React native deployed and currently ongoing

#### Project Description
Caren RPM is a voice interactive platform that makes it easier to record and share health information with healthcare providers and family. You can speak measurements such as blood pressure, glucose, pain and body temperature as well as observations like falls, ER visits, and side effects. Providers and family can instantly view this important health information through a browser on a smartphone, tablet, or computer. This saves time and reduces the need to call, text and email providers and family to keep them informed.

Caren RPM allows users to digitally store health, vision, dental, and Medicare insurance cards, physicians’ business cards, a list of medications, and legal documents. All documents are encrypted and stored in the cloud, allowing users to access their documents via mobile device from anywhere, anytime.

Caren RPM requires minimal steps to complete tasks and is beautifully designed for users with low dexterity, low computer literacy, and physical and mental challenges. Caren RPM is ideal for documenting patients in hospice, with disabilities, or with chronic illnesses such as diabetes and hypertension.

#### Screenshots
Below are some of the Project Screenshots for frontend of the project
Developed using React Native


![alt text](http://rodionsolutions.com/assets/images/caren/caren1.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren2.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren3.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren4.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren5.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren6.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren7.jpg "Order page")

![alt text](http://rodionsolutions.com/assets/images/caren/caren8.jpg "Order page")



#### Running the app

##### For android type the following command "react-native run-android"
##### For ios type the following command "react-native run-ios"

Note: You need to have all the gradle and android or ios tooling installed inorder to be able to run the project.


[Rodion Solutions](http:rodionsolutions.com).
[Email]: info@rodionsolutions.com
